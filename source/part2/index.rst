.. _part2:

*************************************************************************************************
Partie 2 | Parsing
*************************************************************************************************

Ambiguous or not ambiguous, that's the question.
================================================
proposed by Group 16, Trinstan Moers and Simon Lardinois

Question
""""""""

You are given some grammars and you have to determine whether it is ambiguous or 
not. If the grammar is ambiguous, give at least 2 different parse trees.


1. ::

      S  ::= aS | aS'
      S' ::= S'b | Pb
      P  ::= ε

2. ::

      S ::= aS | bS | aSb | P | ε
      P ::= cP | Pd | ε


3. ::

      S ::= aS | P
      P ::= Pb | Q
      Q ::= cQ | R
      R ::= Rd | S | ε

4. ::

      S  ::= aS | aP
      P  ::= bP' | P'a
      P' ::= a  

5. ::

      S  ::= aS | aS'
      S' ::= S'b | Pb | ε
      P  ::= b




Answer
""""""

1. Not ambiguous.

2. Ambiguous : 
   
.. image:: 2_1.png
        :height: 200
        :alt: 2_1

.. image:: 2_2.png
        :height: 200
        :alt: 2_2

3. Ambiguous : 

.. image:: 3_1.png
        :height: 200
        :alt: 3_1

.. image:: 3_2.png
        :height: 200
        :alt: 3_2

4. Not ambiguous.

5. Ambiguous : 

.. image:: 5_1.png
        :height: 200
        :alt: 5_1

.. image:: 5_2.png
        :height: 200
        :alt: 5_2